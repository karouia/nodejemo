var express = require('express');
var server = express() ;
const Etudiant = require('./models/etudiant');
const Jeu = require('./models/jeu');
const Admin = require('./models/admin')
var router = express.Router();
server.engine('html', require('ejs').renderFile);
server.set('view engine', 'html');
const http = require('http');
const hostname = 'localhost';
const port = 4000;
var bodyParser = require("body-parser");
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
const Bcrypt = require("bcryptjs");
var uniqid = require('uniqid');
var cors = require('cors');
server.use(cors());
const nodemailer = require("nodemailer");
const messages = require('./Magic/MagicString');
const numbers = require('./Magic/MagicNumbers');
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const bcryptSalt = process.env.BCRYPT_SALT;

const {
    ajouterSalarieController
}
= require('./controllers/salarie.controller')

const {

    resetPasswordRequestController,
    resetPasswordRequestSalarieController,
    resetPasswordController,
    resetPasswordSalarieController
} = require("./controllers/auth.controller");
const  {
    envoimail_infomodif
} = require("./service/info.service")

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);

});
server.use(express.static(__dirname + '/css'))

const mongoose = require('mongoose');
const Token = require("./models/token");
const sendEmail = require("./utils/email/sendEmail");
mongoose.connect('mongodb+srv://Adam:25muruP6pidzlFyg@cluster0.eygqt.mongodb.net/mindful?retryWrites=true&w=majority',
    { useNewUrlParser: true,
        useUnifiedTopology: true })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

// Page d'accueil
server.get('/app2', function(req, res) {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.end('<html><body>Vous êtes sur la page d\'accueil</body></html>');
})
// Retrouver les étudiants
server.get('/app2/find/:nom', function(req, res) {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    Etudiant.find(  {nomenseignant : req.params.nom} , function(err, etudiants){
        if (err){
            res.send(err);
        }
        let params = {
            liste: etudiants,
            nomprof : req.params.nom
        }
        console.log(params);
        res.render('index.ejs', params);

    });
});
server.get('/app2/enreg/:nom/:mdp/:score/:enseignant/:classe/:parcours',function(req,res){

    let utilisateur = new Etudiant();
    utilisateur.id_salarie = uniqid();
    utilisateur.nom= req.params.nom;
    utilisateur.mdp = Bcrypt.hashSync(req.params.mdp,10);
    utilisateur.score = req.params.score;
    utilisateur.nomenseignant = req.params.enseignant;
    utilisateur.classe = req.params.classe
    utilisateur.activitesdecouvertes = [];
    utilisateur.poi = [];
    utilisateur.reponses = [];
    utilisateur.nom_parcours = req.params.parcours;
    utilisateur.parcours_fini = false;
    utilisateur.showScreen = true;

    Etudiant.find({nom : utilisateur.nom, nomenseignant: utilisateur.nomenseignant}, function ( err, utilisateurs){
        if (utilisateurs.length)
        {
            res.send(messages.erreur_login_meme_identifiant);
        }
        else
        {
            utilisateur.save ( function(err){
                if (err)
                {
                    res.send(err);
                }
                else {
                    res.send(messages.login_reussi)
                }
            });
        }
    });
})
server.get('/app2/login/:nom/:mdp/:nomenseignant/:parcours', function(req,res) {

    try {
        Etudiant.findOne({nom: req.params.nom, nomenseignant: req.params.nomenseignant, nom_parcours: req.params.parcours}, function (err, utilisateur) {
            if (err) {
                res.send({message:err,
                    id_salarie:"0"});
            }
            if (!utilisateur) {
                res.json({message:messages.erreur_login_utilisateur_non_trouve,
                    id_salarie:"0"});
            } else {

                utilisateur.comparePassword(req.params.mdp, function (err, isMatch) {
                    if (isMatch && !err) {
                        // réinitialisation des données
                        utilisateur.score=0;
                        utilisateur.activitesdecouvertes = [];
                        utilisateur.poi = [];
                        utilisateur.datedeb = 0;
                        utilisateur.reponses = [];
                        utilisateur.parcours_fini= false;
                        utilisateur.score_activite = [];
                        utilisateur.temps = [];
                        utilisateur.showScreen = true;
                        utilisateur.save(function(err){
                            if (err){
                                res.send({message :err,
                                      id_salarie:0});
                            }else{
                                res.json({message: messages.connexion_reussie,
                                    id_salarie:utilisateur.id_salarie})
                            }
                        })
                    } else {
                        res.send({message:messages.connexion_echouee,
                            id_salarie:0});
                    }
                })

            }
        }) ;
    } catch (err) {
        console.log(err);
        res.json({
            status: 0,
            message: '500 Internal Server Error',
            data: {}
        })
    }
})
server.get('/app2/score/:id' , function(req,res) {
    console.log(req.params.id)
    Etudiant.findOne({ id_salarie: req.params.id}, function (err,utilisateur) {
        utilisateur.datedeb = Date.now();
        utilisateur.save(function(err){});
        res.json({
            score: utilisateur.score,
            nomprof : utilisateur.nomenseignant,
            showscreen: utilisateur.showScreen
        })
        if (err){
            res.send(err);
        }
    })
})

server.get('/app2/updatescore/:id/:score/:showscreen', function (req,res){


    Etudiant.findOne({ id_salarie: req.params.id}, function (err,utilisateur) {
        if (err){
            res.send(err);
        }
        utilisateur.score_activite.push(req.params.score - utilisateur.score);
        utilisateur.temps.push(Math.floor((Date.now()- utilisateur.datedeb)/1000));
        utilisateur.score=req.params.score;
        utilisateur.showScreen = req.params.showscreen;
        utilisateur.save(function(err){
            if(err){
                res.send(err);
            }
            // Si tout est ok
            res.json({message : messages.modification_reussie});
        });
    })

})

server.get('/app2/activite/:id/:nomactivite', function (req,res){

    Etudiant.findOne({ id_salarie: req.params.id}, function (err,utilisateur) {
        if (err){
            res.send(err);
        }
        for (var i=0; i< utilisateur.activitesdecouvertes.length ;i++){
            if(req.params.nomactivite === utilisateur.activitesdecouvertes[i] ){
                return  res.send('');
            }
        }
        utilisateur.activitesdecouvertes.push(req.params.nomactivite)
        utilisateur.save(function(err){
            if(err){
                res.send(err);
            }
            // Si tout est ok
            res.json({message : messages.modification_reussie});
        });
    })

})
server.get('/app2/poi/:id/:nomactivite', function (req,res){

    Etudiant.findOne({ id_salarie: req.params.id}, function (err,utilisateur) {
        if (err){
            res.send(err);
        }
        for (var i=0; i< utilisateur.poi.length ;i++){
            if(req.params.nomactivite === utilisateur.poi[i] ){
                return  res.send('')
            }
        }
        utilisateur.poi.push(req.params.nomactivite)
        utilisateur.save(function(err){
            if(err){
                res.send(err);
            }
            // Si tout est ok
            res.json({message : messages.modification_reussie});
        });
    })
})
server.get('/app2/reponse/:id/:reponse', function (req,res){
    Etudiant.findOne({ id_salarie: req.params.id},function (err,utilisateur){
        if (err){
            res.send(err);
        }
        let reponse_formatee = utilisateur.activitesdecouvertes[utilisateur.activitesdecouvertes.length-1] + " : " +  req.params.reponse;
        utilisateur.reponses.push(reponse_formatee)
        utilisateur.save(function(err){
            if(err){
                res.send(err);
            }
            // Si tout est ok
            res.json({message : messages.modification_reussie});
        });
    })
})
server.get('/app2/moyennetemps/:nom/:nomparcours', function (req,res) {
    Etudiant.find( {nomenseignant:req.params.nom, nom_parcours : req.params.nomparcours}, function (err,etudiants) {
        if (err){
            res.send(err);
        }
        let nombre_etudiants = 0
        let nombre_activites = 0
        let sommetemps = 0;
        for(let i=0; i<etudiants.length;i++){
            let somme_temps_etu = 0
            for (j=0;j<etudiants[i].temps.length;j++){
                somme_temps_etu= somme_temps_etu +Math.floor(etudiants[i].temps[j]);
                nombre_activites++;

            }
            sommetemps= sommetemps + somme_temps_etu
            nombre_etudiants++
        }

        let params = {moyennetemps : Math.floor(sommetemps/nombre_etudiants),
            moyennetempsparactivite : Math.floor(sommetemps/nombre_activites)}
        res.json(params);
    });
});
server.get('/app2/moyennescore/:nom/:nomparcours' , function (req,res) {
    Etudiant.find( {nomenseignant:req.params.nom, nom_parcours : req.params.nomparcours}, function (err,etudiants) {
        if (err){
            res.send(err);
        }
        let nombre_etudiants = 0
        let nombre_activites = 0
        let sommescore = 0;
        for( let i=0; i<etudiants.length;i++){
            let somme_score_etu = 0
            for (let j=0;j<etudiants[i].score_activite.length;j++){
                somme_score_etu= somme_score_etu +etudiants[i].score_activite[j];
                nombre_activites++;
            }
            sommescore= sommescore + somme_score_etu
            nombre_etudiants++
        }

        let params = {moyennescore : sommescore/nombre_etudiants,
            moyennescoreparactivite : sommescore/nombre_activites}
        res.json(params);
    });
})


server.get('/app2/tauxreussite/:nom/:nomparcours' , function (req,res){
    Jeu.findOne({nomAdmin:req.params.nom, nomJeu : req.params.nomparcours}, function(err,jeu){
        let nombre_activites = jeu.nombre_activites
        Etudiant.find( {nomenseignant:req.params.nom, nom_parcours : req.params.nomparcours}, function(err,etudiants) {
            if (err) {
                res.send(err);
            }
            let nombre_etudiants =0
            let nombre_etudiants_tout_reussi =0
            let nombre_etudiants_partiel_reussi = 0
            for(i=0; i<etudiants.length;i++) {
                nombre_etudiants++
                if ( etudiants[i].parcours_fini){
                    nombre_etudiants_partiel_reussi++
                }
                if (!etudiants[i].score_activite.includes(0) && etudiants[i].score_activite.length === nombre_activites) {
                    nombre_etudiants_tout_reussi++
                }
            }
            let taux_complet_reussite= Math.floor((nombre_etudiants_tout_reussi/nombre_etudiants)*100);
            let taux_partiel_reussite = Math.floor((nombre_etudiants_partiel_reussi/nombre_etudiants)*100)
            let params = {
                taux_partiel_reussite,
                taux_partiel_echec : 100 - taux_partiel_reussite,
                taux_complet_reussite,
                taux_complet_echec : 100-taux_complet_reussite

            }
            res.send(params);
        });
    })
})
server.get('/app2/taux_reussite_activite/:nom/:nomparcours', function (req,res){
    Jeu.findOne({nomAdmin:req.params.nom, nomJeu : req.params.nomparcours}, function(err,jeu){
        let nombre_activites = jeu.nombre_activites


        Etudiant.find( {nomenseignant:req.params.nom, nom_parcours: req.params.nomparcours}, function(err,etudiants) {
            if (err) {
                res.send(err);
            }
            let nombre_etudiants=0
            let reussite_par_activite = []
            let tauxreussite_par_activite =[]

            for (let i=0;i<etudiants.length;i++){
                let reussite_activite =0
                for (let j=0; j < etudiants[i].score_activite.length;j++){
                    if(etudiants[i].score_activite[j]!==0){
                        reussite_activite++
                    }

                }
                reussite_par_activite[i]=reussite_activite
                nombre_etudiants++
            }
            for(let k=0; k< reussite_par_activite.length;k++){
                tauxreussite_par_activite[k]= Math.floor(reussite_par_activite[k]/nombre_etudiants*100)
            }
            let params=  { tauxreussite_par_activite, nombre_activites }
            res.json(params)
        })
    })
})
server.get('/app2/tableau/:nom/:nomparcours', function(req,res){
    Etudiant.find( {nomenseignant:req.params.nom, nom_parcours : req.params.nomparcours}, function(err,etudiants) {
        if (err) {
            res.send(err);
        }
        let stringHtml = "";
        let numeroActivite="1";
        let tableau_etudiants =[]
        for(i=0;i<etudiants.length;i++){
            for (j=0; j<etudiants[i].reponses.length;j++){
                if(j===0){
                    stringHtml=""
                    numeroActivite = etudiants[i].reponses[j].substring(numbers.reponses_debut_num_activite,numbers.reponses_fin_num_activite)
                    stringHtml = stringHtml.concat(etudiants[i].reponses[j].substring(0,numbers.reponses_fin_num_activite), "<ul><li>", etudiants[i].reponses[j].substring(numbers.reponses_debut_contenu) ,"</li>" );

                }
                else {
                    if (etudiants[i].reponses[j].substring(numbers.reponses_debut_num_activite,numbers.reponses_fin_num_activite)=== numeroActivite){
                        stringHtml =  stringHtml.concat("<li>", etudiants[i].reponses[j].substring(numbers.reponses_debut_contenu), "</li>")
                    }
                    else {
                        stringHtml = stringHtml.concat("</ul>", etudiants[i].reponses[j].substring(0,numbers.reponses_fin_num_activite) , "<ul><li>", etudiants[i].reponses[j].substring(numbers.reponses_debut_contenu) ,"</li>" )
                        numeroActivite = etudiants[i].reponses[j].substring(numbers.reponses_debut_num_activite,numbers.reponses_fin_num_activite)
                    }
                }
                if (j=== etudiants[i].reponses.length - 1){
                    stringHtml = stringHtml.concat("</ul>" )
                }
            }

            let etudiant=
                {
                    id_Salarie:etudiants[i].nom,
                    Service: etudiants[i].classe,
                    Score:etudiants[i].score,
                    Activites_debloquees:etudiants[i].activitesdecouvertes,
                    POI_trouves:etudiants[i].poi,
                    reponse:stringHtml}

            tableau_etudiants.push(etudiant);

        }

        res.json(tableau_etudiants)
    })
})

server.post('/app2/postAdmin' , function(req,res){
    let admin = new Admin()
    admin.id = uniqid()
    admin.nom = req.body.nom;
    admin.mail = req.body.mail
    admin.mdp= Bcrypt.hashSync(req.body.mdp,10);
    admin.liste_parcours=[]
    admin.save(function(err){
        if (err){
            res.send(err);
        }
        res.send({message: admin})
    })
})
server.put('/app2/compile_parcours' , function(req,res){
    res.header("Access-Control-Allow-Origin", "*");

    console.log(req.body)
    Admin.findOne( {mail:req.body.mailAdmin}, function(err,admin) {
        const nomadmin = admin.nom
        if (err){
            res.send(err);
        }
        if(admin.liste_parcours.includes(req.body.nom_parcours)){

            Jeu.findOne({nomJeu: req.body.nom_parcours, nomAdmin : nomadmin} , function(err,jeu){
                if (err){
                    res.send(err);
                }
                let today = new Date();
                jeu.nombre_activites = req.body.nombre_activites;

                jeu.jour_modif = today.getDate();
                jeu.mois_modif = today.getMonth();
                jeu.annee_modif = today.getFullYear();
                console.log(jeu)
                jeu.save(function(err){
                    if (err){
                        res.send(err);
                    }
                })
                res.send(messages.modification_reussie)
            })
        } else {
            admin.liste_parcours.push(req.body.nom_parcours)
            admin.save(function(err){
                if (err){
                    res.send(err);
                }
            })
            var parcours = new Jeu()
            parcours.idJeu= uniqid()
            parcours.nomJeu= req.body.nom_parcours
            parcours.nomAdmin = nomadmin
            parcours.nombre_activites = req.body.nombre_activites;
            let today = new Date()
            parcours.jour_creation = today.getDate();
            parcours.mois_creation = today.getMonth();
            parcours.annee_creation = today.getFullYear();
            parcours.jour_modif = today.getDate();
            parcours.mois_modif = today.getMonth();
            parcours.annee_modif = today.getFullYear()

            parcours.save(function(err){
                if (err){
                    res.send(err)
                }
            })
            res.send(messages.modification_reussie)
        }

    })
})
server.get('/app2/tableau_parcours/:nom', function(req,res){
    Jeu.find( {nomAdmin:req.params.nom}, function(err,parcours) {
        if (err) {
            res.send(err);
        }
        let tableau_parcours =[]
        for(i=0;i<parcours.length;i++){
            let Date_de_creation = new Date( parcours[i].annee_creation, parcours[i].mois_creation, parcours[i].jour_creation )
            let options = { year: "numeric", month: "long", day: "numeric"}
            Date_de_creation = Date_de_creation.toLocaleDateString("fr-Fr", options)
            let Date_de_modification = new Date(parcours[i].annee_modif,parcours[i].mois_modif, parcours[i].jour_modif,)
            Date_de_modification = Date_de_modification.toLocaleDateString("fr-Fr", options)
            let parcour=
                {
                    Nom_parcours:parcours[i].nomJeu,
                    Nombre_activites:parcours[i].nombre_activites,
                    Date_de_creation : Date_de_creation,
                    Date_de_modification : Date_de_modification
                }

            tableau_parcours.push(parcour);

        }

        res.json(tableau_parcours)
    })
})
server.get('/app2/loginAdmin/:nom/:pwd', function(req,res){
    try {
        Admin.findOne({nom: req.params.nom}, function (err, etudiant) {
            if (err) {
                res.send({message : messages.erreur_login_utilisateur_non_trouve,
                                super : 'false'});
            }
            if (!etudiant) {
                res.send(messages.erreur_login_utilisateur_non_trouve);
            } else {
                // check if password matches
                etudiant.comparePassword(req.params.pwd, function (err, isMatch) {
                    if (isMatch && !err) {
                        // if user is found and password is right create a token
                        if (etudiant.super === true){
                            res.send({
                                message : messages.connexion_reussie,
                                super : 'true'
                            })
                        }
                        else{
                            res.send({
                            message : messages.connexion_reussie,
                            super : 'false'
                            });
                        }
                    } else {
                        res.send( {message:messages.erreur_login_mauvais_mdp}  );
                    }
                });
            }
        });
    } catch (err) {
        console.log(err);
        res.json({
            status: 0,
            message: '500 Internal Server Error',
            data: {}
        })
    }
})
server.get('/app2/scorefinal/:id', function(req,res){

    Etudiant.findOne({id_salarie: req.params.id}, function(err,etudiant){
        etudiant.parcours_fini = true
        etudiant.save(function(err){
            if(err){
                res.send(err)
            }
            res.send(etudiant.score.toString())
        })

    })
})
server.delete('/app2/parcours', function(req,res){

    Jeu.deleteOne({nomAdmin: req.body.username, nomJeu: req.body.nom_parcours}, function(err){
        if(err){
            res.send(err)
        }
    })
})
server.delete('/app2/salarie/delete', function(req,res){
    console.log(req.body)
    Etudiant.deleteOne({nomenseignant: req.body.username, nom_parcours: req.body.nom_parcours, nom:req.body.nom}, function(err){
        if(err){
            res.send(err)
        }
    })
})
server.get('/app2/invite/:parcours', function(req,res){
    Etudiant.findOne({nom: 'invite', nom_parcours: req.params.parcours}, function (err, utilisateur) {
        if(!utilisateur){
            var  etudiant = new Etudiant();
            etudiant.id_salarie = uniqid();
            etudiant.nom= 'invite';
            etudiant.mdp = Bcrypt.hashSync('mdp',10);
            etudiant.score = 0;
            etudiant.nomenseignant = 'invite';
            etudiant.classe = '0';
            etudiant.activitesdecouvertes = [];
            etudiant.poi = [];
            etudiant.reponses = [];
            etudiant.nom_parcours = req.params.parcours;
            etudiant.parcours_fini= false
            etudiant.showScreen = true;
            etudiant.save(function(err){
                if(err){
                    res.send(err);
                }
                // Si tout est ok
                res.send(etudiant.id_salarie);
            });
        }
        else{

            utilisateur.score=0;
            utilisateur.activitesdecouvertes = [];
            utilisateur.poi = [];
            utilisateur.reponses = [];
            utilisateur.parcours_fini= false;
            utilisateur.score_activite = [];
            utilisateur.temps = [];
            utilisateur.showScreen = true;
            utilisateur.save(function(err){
                if(err){
                    console.log(err);
                    res.send(err);
                }
                else{
                // Si tout est ok
                res.send(utilisateur.id_salarie);
                }
            });
        }

    })
})
server.post('/app2/mailAdmin', function(req,res){

    Admin.findOne({nom: req.body.data.nom}, function(err,admin){
        if(!admin){
            res.send("");
        }
        else{
            res.send(admin.mail)
        }
    })
})
server.put('/app2/changermotdepasse',function(req,res){
    Admin.findOne({nom:req.body.data.nom}, function(err,admin){
        if(!admin){
            res.send("Il y a eu une erreur lors du traitement des données, veuillez vous reconnecter");
        }
        else{
            admin.comparePassword(req.body.data.ancienmdp, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    admin.mdp = Bcrypt.hashSync(req.body.data.nouveaumdp,10);
                    admin.save(function(err){
                        if(err){
                            res.send(err);
                        }else{
                            // Si tout est ok
                            res.send(messages.modification_reussie_dashboard);}
                    });

                } else {
                    res.send(messages.erreur_modif_mauvais_mdp);
                }
            })
        }
    })
})
server.post('/app2/logo',function(req,res){
    Admin.findOne({nom:req.body.data.nom},function(err,admin){
        res.send(admin.logo[0]);
    })
})
server.put('/app2/identifiant',function(req,res){
    Admin.findOne({nom: req.body.data.nouveaunom},function(err,admin){
        if(admin){
            res.send( messages.erreur_modif_meme_id);
        }
        else {
            Admin.findOne({nom:req.body.data.anciennom},function(err,admin2){
                admin2.nom = req.body.data.nouveaunom;
                admin2.save();
                Jeu.updateMany({nomAdmin:req.body.data.anciennom}, {nomAdmin: req.body.data.nouveaunom},function(
                    err,
                    result
                ) {
                    if (err) {
                        ;
                    } else {
                        ;
                    }});

                res.send(messages.modification_reussie_dashboard);
            })
        }
    })

})

server.put('/app2/mail', function(req,res){

    Admin.findOne({mail: req.body.data.nouveaumail},function(err,admin){
        if(admin){
            res.send(messages.erreur_modif_meme_mail);
        }else {
            Admin.findOne({mail:req.body.data.ancienmail},function(err,admin2){
                admin2.mail = req.body.data.nouveaumail;
                admin2.save();

                res.send(messages.modification_reussie_dashboard)
            })
        }
    })
})
server.post('/app2/mailmdp',function(req,res){

    const mail =req.body.data.mail;
    let response = envoimail_infomodif(mail);
    res.json(response)
})
server.post('/app2/getListeEmploye', function (req,res){
    Etudiant.find({nomenseignant: req.body.userId , nom_parcours: req.body.parcours} , function (err,salaries) {
        let tableau_salaries =[];
        for (let i = 0; i< salaries.length; i++)
        {
            if (!salaries[i].mail){
                salaries[i].mail = "Non renseigné";
            }
            let salarie = {
                Identifiant: salaries[i].nom,
                Service: salaries[i].classe,
                Mail: salaries[i].mail
            };
            tableau_salaries.push(salarie);

        }

        res.json(tableau_salaries);
    })
})
server.put('/app2/updateEmployemail', function (req,res) {

    Etudiant.findOne({nom:req.body.EmployeId ,nomenseignant: req.body.userId, nom_parcours: req.body.parcours}, function (err,salarie){
        salarie.mail = req.body.mail;
        salarie.save(function(err){
            if (err){
                res.send(err);
            }else{
                res.json(" Modification effectuée")
            }
        })
    })
})
server.post('/app2/getListeAdmin' , async function (req,res) {

    let liste_admins = await Admin.find({nom_super : req.body.superadmin })

    let liste_envoie = []
        for(let i=0; i< liste_admins.length; i++){
            liste_envoie.push( {
                Identifiant: liste_admins[i].nom,
                Mail :liste_admins[i].mail
            })
        }
    res.json(liste_envoie)
})
server.post('/app2/addAdmin' , async function (req,res){
    let verif_nom = await Admin.findOne({nom : req.body.nom})
    console.log(verif_nom)
    if(verif_nom){
        res.json("Cet identifiant est déjà pris")
        return
    }

    let verif_mail = await Admin.findOne({mail : req.body.mail})
    if (verif_mail) {
        res.json("Cette addresse mail est déjà prise")
        return
    }
        await new Admin({
            id : uniqid(),
            mail : req.body.mail,
            nom : req.body.nom,
            mdp :  Bcrypt.hashSync(uniqid(),10),
            liste_parcours : [],
            logo : [],
            super : false,
            nom_super: req.body.nom_super
        }).save();
    let resetToken = crypto.randomBytes(32).toString("hex");
    const hash = await bcrypt.hash(resetToken, Number(bcryptSalt));
    let utilisateur = await Admin.findOne({mail: req.body.mail})
    await new Token({
        userId: utilisateur.id,
        token: hash,
        createdAt: Date.now(),
    }).save();

    const image = { src :'Capture.png' , name:'capture'}
    const link = `mindful-soft.com/#/passwordReset/${resetToken}/${utilisateur.id}/admin`;
    sendEmail(utilisateur.mail,"Modification de mot de passe",{name: utilisateur.nom,link: link, image:image},"./template/requestResetPassword.handlebars");
    res.json("Un e-mail de confirmation a été envoyé à l'adresse indiquée.")
})
server.delete('/app2/deleteAdmin', async function (req,res){
    let admin_delete = await Admin.findOne({nom: req.body.nom})
    admin_delete.deleteOne()
    res.json("Compte supprimé")
})
server.post("/app2/getListeParcoursSuper", async function (req,res){
    let admin = await Admin.find({nom_super: req.body.username});

    let tableau_parcours = [];
    for (let i=0; i< admin.length;i++){
        let jeux = await Jeu.find({nomAdmin: admin[i].nom})


        for(let j=0; j<jeux.length;j++){

            let Date_de_creation = new Date( jeux[j].annee_creation, jeux[j].mois_creation, jeux[j].jour_creation );
            let options = { year: "numeric", month: "long", day: "numeric"}
            Date_de_creation = Date_de_creation.toLocaleDateString("fr-Fr", options)
            let Date_de_modification = new Date(jeux[j].annee_modif,jeux[j].mois_modif, jeux[j].jour_modif,)
            Date_de_modification = Date_de_modification.toLocaleDateString("fr-Fr", options)
            tableau_parcours.push({

                Nom_parcours : jeux[j].nomJeu,
                Nom_admin : jeux[j].nomAdmin,
                Nombre_activites : jeux[j].nombre_activites,
                Date_de_creation,
                Date_de_modification,

            })
        }

    }
    res.json(tableau_parcours)
})
server.post("/app2/getListeSalariesSuper", async function (req,res){
    let admin = await Admin.find({nom_super: req.body.username});

    let tableau_salaries = [];
    for (let i=0; i< admin.length;i++){
        let salaries = await Etudiant.find({nomenseignant: admin[i].nom})


        for(let j=0; j<salaries.length;j++){

            tableau_salaries.push({

                Nom_Salarie : salaries[j].nom,
                Service : salaries[j].classe,
                Nom_admin : salaries[j].nomenseignant,
                Nom_parcours: salaries[j].nom_parcours,
                Adresse_mail : salaries[j].mail



            })
        }

    }
    res.json(tableau_salaries)
})


server.post("/app2/requestResetPassword", resetPasswordRequestController)
server.post("/app2/passwordReset" , resetPasswordController)
server.post("/app2/requestResetSalariePassword", resetPasswordRequestSalarieController)
server.post("/app2/passwordSalarieReset", resetPasswordSalarieController)
server.post("/app2/salarie/ajouterSalarie", ajouterSalarieController)
// Page inconnue
server.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain; charset=utf-8');
    res.status(404).send('Vous êtes sur une page inconnue.');
});
