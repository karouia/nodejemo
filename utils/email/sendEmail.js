const nodemailer = require("nodemailer");
const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");

const sendEmail = async (email, subject, payload, template) => {
    try {
        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            service: "Gmail",
            auth: {
                user: "testmindfulhouse@gmail.com",
                pass: "stagejemo"
            }
        });

        const source = fs.readFileSync(path.join(__dirname, template), "utf8");
        const compiledTemplate = handlebars.compile(source);
        const options = () => {
            return {
                from: process.env.FROM_EMAIL,
                to: email,
                subject: subject,
                attachments: [{
                    filename: 'Capture.png',
                    path: __dirname +'/template/Capture.png',
                    cid: 'Capture.png'
                }],
                html: compiledTemplate(payload),


            };
        };

        // Send email
        transporter.sendMail(options(), (error, info) => {
            if (error) {
                return error;
            } else {
                console.log(info.response)
                return res.status(200).json({
                    success: true,
                });
            }
        });
    } catch (error) {
        return error;
    }
};

/*
Example:
sendEmail(
  "youremail@gmail.com,
  "Email subject",
  { name: "Eze" },
  "./templates/layouts/main.handlebars"
);
*/

module.exports = sendEmail;
