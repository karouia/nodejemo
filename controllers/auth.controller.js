const {

    requestPasswordReset,
    resetPassword,
    requestPasswordSalarieReset,
    resetPasswordSalarie,
} = require("../service/auth.service");

const signUpController = async (req, res, next) => {
    const signupService = await signup(req.body);
    return res.json(signupService);
};

const resetPasswordRequestController = async (req, res, next) => {
    const requestPasswordResetService = await requestPasswordReset(
        req.body.email
    );
    return res.json(requestPasswordResetService);
};
const resetPasswordRequestSalarieController = async (req, res, next) => {
    const requestPasswordResetSalarieService = await requestPasswordSalarieReset(
        req.body.nom,
        req.body.nomparcours
    );
    return res.json(requestPasswordResetSalarieService);
};

const resetPasswordController = async (req, res, next) => {
    const resetPasswordService = await resetPassword(
        req.body.userId,
        req.body.token,
        req.body.password
    );
    return res.json(resetPasswordService);
};
const resetPasswordSalarieController = async (req, res, next) => {
    const resetPasswordSalarieService = await resetPasswordSalarie(
        req.body.userId,
        req.body.token,
        req.body.password
    );
    return res.json(resetPasswordSalarieService);
};

module.exports = {

  resetPasswordRequestController,
  resetPasswordRequestSalarieController,
  resetPasswordController,
  resetPasswordSalarieController
};
