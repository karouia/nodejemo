const messages = {

    erreur_login_meme_identifiant:'Identifiant déjà utilisé pour ce parcours',
    login_reussi : 'Enregistrement reussi',
    erreur_login_utilisateur_non_trouve : 'Authentication échouée. Utilisateur non trouvé.',
    connexion_reussie: 'Connexion réussie ',
    connexion_echouee: 'Connexion echouée ',
    modification_reussie : 'Bravo, mise à jour des données OK',
    erreur_login_mauvais_mdp:'Authentication echouée. Mauvais mot de passe.',
    modification_reussie_dashboard: 'Modification réussie',
    erreur_modif_mauvais_mdp: 'Mauvais mot de passe',
    erreur_modif_meme_id: "Cet identifiant est déjà pris par un autre utilisateur",
    erreur_modif_meme_mail: "Cette addresse mail est déjà prise par un autre utilisateur",
    sujet_mail_modif_mdp : "Changement de mdp (no-reply)",
    text_mail_modif_mdp : "Le changement de mot de passe a été effectuée",

}


module.exports = messages