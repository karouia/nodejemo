const JWT = require("jsonwebtoken");
const Admin = require("../models/admin");
const Etudiant = require('../models/etudiant')
const Token = require("../models/token");
const sendEmail = require("../utils/email/sendEmail");
const crypto = require("crypto");
const bcrypt = require("bcrypt");

const JWTSecret = process.env.JWT_SECRET;
const bcryptSalt = process.env.BCRYPT_SALT;
const clientURL = process.env.CLIENT_URL;

const requestPasswordReset = async (email) => {

    const user = await Admin.findOne({ mail: email });

    if (!user) return "L'utilisateur n'existe pas";
    let token = await Token.findOne({ userId: user.id });
    if (token) await token.deleteOne();
    let resetToken = crypto.randomBytes(32).toString("hex");
    const hash = await bcrypt.hash(resetToken, Number(bcryptSalt));

    await new Token({
        userId: user.id,
        token: hash,
        createdAt: Date.now(),
    }).save();

    const link = `mindful-soft.com/#/passwordReset/${resetToken}/${user.id}/admin`;
    sendEmail(user.mail,"Password Reset Request",{name: user.name,link: link,},"./template/requestResetPassword.handlebars");
    return "Un email vous a été envoyé pour réinitialiser votre mot de passe";
};
const requestPasswordSalarieReset = async (nomuser, nomparcours) => {

    const user = await Etudiant.findOne({ nom: nomuser, nom_parcours: nomparcours });

    if (!user) return "L'utilisateur n'existe pas.";
    let token = await Token.findOne({ userId: user.id_salarie });
    if (token) await token.deleteOne();
    let resetToken = crypto.randomBytes(32).toString("hex");
    const hash = await bcrypt.hash(resetToken, Number(bcryptSalt));

    await new Token({
        userId: user.id_salarie,
        token: hash,
        createdAt: Date.now(),
    }).save();

    const link = `mindful-soft.com/#/passwordReset/${resetToken}/${user.id_salarie}/salarie`;
    console.log(link)
    const image = { src :'Capture.png' , name:'capture'}
    sendEmail(user.mail,"Password Reset Request",{name: user.nom,link: link, image:image},"./template/requestResetPassword.handlebars");
    return "Un email vous a été envoyé pour réinitialiser votre mot de passe";
};

const resetPassword = async (userId, token, password) => {
    let passwordResetToken = await Token.findOne({ userId });
    if (!passwordResetToken) {
        return ("La demande a expirée, veuillez recommencer depuis le début de la procédure");
    }
    const isValid = await bcrypt.compare(token, passwordResetToken.token);
    if (!isValid) {
        return ("La demande a expirée, veuillez recommencer depuis le début de la procédure");
    }
    const hash = await bcrypt.hash(password, Number(bcryptSalt));
    await Admin.updateOne(
        { id: userId },
        { $set: { mdp: hash } },
        { new: true }
    );
    const user = await Admin.findOne({ id: userId });
    sendEmail(
        user.mail,
        "Mot de passe réinitialisé",
        {
            name: user.nom,
        },
        "./template/informationsModifiees.handlebars"
    );
    await passwordResetToken.deleteOne();
    return true;
};

const resetPasswordSalarie = async (userId, token, password) => {
    console.log(userId)
    console.log(token)
    let passwordResetToken = await Token.findOne({ userId });
    if (!passwordResetToken) {
        return ("La demande a expirée, veuillez recommencer depuis le début de la procédure");
    }
    const isValid = await bcrypt.compare(token, passwordResetToken.token);
    if (!isValid) {
        return ("La demande a expirée, veuillez recommencer depuis le début de la procédure");
    }
    const hash = await bcrypt.hash(password, Number(bcryptSalt));
    console.log("a")
    await Etudiant.updateOne(
        { id_salarie: userId },
        { $set: { mdp: hash } },
        { new: true }
    );
    const user = await Etudiant.findOne({ id_salarie: userId });
    sendEmail(
        user.mail,
        "Mot de passe réinitialisé",
        {
            name: user.nom,
        },
        "./template/informationsModifiees.handlebars"
    );
    await passwordResetToken.deleteOne();
    return "Le mot de passe a été réinitialisé.";
};

module.exports = {

    requestPasswordReset,
    requestPasswordSalarieReset,
    resetPassword,
    resetPasswordSalarie,
};
