const mongoose = require('mongoose');
const Bcrypt = require("bcryptjs");

const etudiantSchema = mongoose.Schema({
    id_salarie : { type : String, required: true},
    nom: { type: String, required: true },
    mdp: {type: String ,required : true},
    score: {type: Number, required: true },
    nomenseignant : {type: String, required: true },
    activitesdecouvertes :{ type : [String], required : true},
    poi : { type : [String], required : true},
    datedeb : {type : Number, required : false},
    temps : {type : [Number], required: false},
    reponses : { type : [String], required : true},
    score_activite: { type: [Number], required: false},
    nom_parcours: { type:String, required : true},
    parcours_fini: {type:Boolean, required : true},
    classe: { type: String, required: true},
    showScreen: {type : Boolean, required : true},
    mail: {type:String, required:false}
},
{ versionKey : false});
etudiantSchema.methods.comparePassword = function(etudiantmdp, cb) {
    Bcrypt.compare(etudiantmdp, this.mdp, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
module.exports = mongoose.model('Etudiant', etudiantSchema);
