const mongoose = require('mongoose');
const Bcrypt = require("bcryptjs");

const adminSchema = mongoose.Schema({
  id : { type : String, required: true},
  mail : { type : String, required : true},
  nom: { type: String, required: true },
  mdp: {type: String , required : true},
  liste_parcours : {type : [String] , required : false},
  logo : { type: [String],required: false},
  super: {type: Boolean, required: false},
  nom_super : {type: String, required:false}

},
{ versionKey : false});
adminSchema.methods.comparePassword = function(adminmdp, cb) {
    Bcrypt.compare(adminmdp, this.mdp, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('Admin', adminSchema);
