const mongoose = require('mongoose');

const jeuSchema = mongoose.Schema({
  idJeu : {type : String , required : true},
  nomJeu: {type : String , required: true},
  nomAdmin:  {type : String , required: true},
  nombre_activites : {type : Number, required : true},
  jour_creation: {type:Number, required:false},
  mois_creation: {type:Number, required:false},
  annee_creation: {type:Number, required:false},
  jour_modif: {type:Number, required:false},
  mois_modif: {type:Number, required:false},
  annee_modif: {type:Number, required:false},
},
{ versionKey : false});
module.exports = mongoose.model('Jeu', jeuSchema);
